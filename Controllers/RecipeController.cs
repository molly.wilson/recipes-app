using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using recipes.Models;

namespace recipes.Controllers;

[ApiController]
[Route("[controller]")]
public class RecipeController : ControllerBase
{
    private readonly RecipeDBContext _context;

    public RecipeController(RecipeDBContext context)
    {
        _context = context;
    }

    [HttpGet]
    public IEnumerable<Recipe> Get()
    {
        return _context.Recipe
            .Include(recipe => recipe.method)
            .Include(recipe => recipe.ingredients)
            .ThenInclude(ingredient => ingredient.foodItem)
            .ToList<Recipe>();
    }

    [HttpGet("{id}")]
    public ActionResult<Recipe> GetRecipe(int id)
    {
        var recipe = _context.Recipe.Find(id);

        if (recipe == null)
        {
            return NotFound();
        }

        return recipe;
    }

    [HttpPut("{id}")]
    public ActionResult<Recipe> Put(int id, Recipe recipe)
    {
        if (id != recipe.id)
        {
            return BadRequest();
        }

        _context.Entry(recipe).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

        try
        {
            _context.SaveChanges();
        }
        catch (Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException)
        {
            if (!RecipeExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        return NoContent();
    }


    [HttpPost]
    public CreatedAtActionResult Post(Recipe recipe)
    {
        _context.Recipe.Add(recipe);
        _context.SaveChanges();

        return CreatedAtAction("Get", new { id = recipe.id }, recipe);
    }

    [HttpDelete("{id}")]
    public ActionResult<Recipe> Delete(int id)
    {
        var recipe = _context.Recipe.Find(id);
        if (recipe == null)
        {
            return NotFound();
        }

        _context.Recipe.Remove(recipe);
        _context.SaveChanges();

        return NoContent();
    }

    private bool RecipeExists(int id)
    {
        return _context.Recipe.Any(e => e.id == id);
    }
}
