using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using recipes.Models;

namespace recipes.Controllers;

[ApiController]
[Route("[controller]")]
public class IngredientController : ControllerBase
{
    private readonly RecipeDBContext _context;

    public IngredientController(RecipeDBContext context)
    {
        _context = context;
    }

    [HttpGet]
    public IEnumerable<Ingredient> Get()
    {
        return _context.Ingredient
        .Include(ingredient => ingredient.foodItem)
        .ToList<Ingredient>();
    }

    [HttpGet("{id}")]
    public ActionResult<Ingredient> GetRecipe(int id)
    {
        var recipe = _context.Ingredient.Find(id);

        if (recipe == null)
        {
            return NotFound();
        }

        return recipe;
    }

    [HttpPut("{id}")]
    public ActionResult<Ingredient> Put(int id, Ingredient ingredient)
    {
        if (id != ingredient.id)
        {
            return BadRequest();
        }

        _context.Entry(ingredient).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

        try
        {
            _context.SaveChanges();
        }
        catch (Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException)
        {
            if (!IngredientExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        return NoContent();
    }


    [HttpPost]
    public CreatedAtActionResult Post(Ingredient ingredient)
    {
        _context.Ingredient.Add(ingredient);
        _context.SaveChanges();

        return CreatedAtAction("Get", new { id = ingredient.id }, ingredient);
    }

    [HttpDelete("{id}")]
    public ActionResult<Ingredient> Delete(int id)
    {
        var Ingredient = _context.Ingredient.Find(id);
        if (Ingredient == null)
        {
            return NotFound();
        }

        _context.Ingredient.Remove(Ingredient);
        _context.SaveChanges();

        return NoContent();
    }

    private bool IngredientExists(int id)
    {
        return _context.Ingredient.Any(e => e.id == id);
    }
}
