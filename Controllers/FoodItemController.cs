using Microsoft.AspNetCore.Mvc;
using System.Data.Entity.Infrastructure;
using recipes.Models;

namespace recipes.Controllers;

[ApiController]
[Route("[controller]")]
public class FoodItemController : ControllerBase
{
    private readonly RecipeDBContext _context;

    public FoodItemController(RecipeDBContext context)
    {
        _context = context;
    }

    [HttpGet]
    public IEnumerable<FoodItem> Get()
    {
        return _context.FoodItem.ToList<FoodItem>();
    }

    [HttpGet("{id}")]
    public ActionResult<FoodItem> GetRecipe(int id)
    {
        var recipe = _context.FoodItem.Find(id);

        if (recipe == null)
        {
            return NotFound();
        }

        return recipe;
    }

    [HttpPut("{id}")]
    public ActionResult<FoodItem> Put(int id, FoodItem foodItem)
    {
        if (id != foodItem.id)
        {
            return BadRequest();
        }

        _context.Entry(foodItem).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

        try
        {
            _context.SaveChanges();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!FoodItemExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        return NoContent();
    }


    [HttpPost]
    public CreatedAtActionResult Post(FoodItem foodItem)
    {
        _context.FoodItem.Add(foodItem);
        _context.SaveChanges();

        return CreatedAtAction("Get", new { id = foodItem.id }, foodItem);
    }

    [HttpDelete("{id}")]
    public ActionResult<FoodItem> Delete(int id)
    {
        var FoodItem = _context.FoodItem.Find(id);
        if (FoodItem == null)
        {
            return NotFound();
        }

        _context.FoodItem.Remove(FoodItem);
        _context.SaveChanges();

        return NoContent();
    }

    private bool FoodItemExists(int id)
    {
        return _context.FoodItem.Any(e => e.id == id);
    }
}
