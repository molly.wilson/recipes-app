using Microsoft.AspNetCore.Mvc;
using System.Data.Entity.Infrastructure;
using recipes.Models;

namespace recipes.Controllers;

[ApiController]
[Route("[controller]")]
public class StepController : ControllerBase
{
    private readonly RecipeDBContext _context;

    public StepController(RecipeDBContext context)
    {
        _context = context;
    }

    [HttpGet]
    public IEnumerable<Step> Get()
    {
        return _context.Step.ToList<Step>();
    }

    [HttpGet("{id}")]
    public ActionResult<Step> GetRecipe(int id)
    {
        var recipe = _context.Step.Find(id);

        if (recipe == null)
        {
            return NotFound();
        }

        return recipe;
    }

    [HttpPut("{id}")]
    public ActionResult<Step> Put(int id, Step step)
    {
        if (id != step.id)
        {
            return BadRequest();
        }

        _context.Entry(step).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

        try
        {
            _context.SaveChanges();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!StepExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }
        return NoContent();
    }


    [HttpPost]
    public CreatedAtActionResult Post(Step step)
    {
        _context.Step.Add(step);
        _context.SaveChanges();

        return CreatedAtAction("Get", new { id = step.id }, step);
    }

    [HttpDelete("{id}")]
    public ActionResult<Step> Delete(int id)
    {
        var Step = _context.Step.Find(id);
        if (Step == null)
        {
            return NotFound();
        }

        _context.Step.Remove(Step);
        _context.SaveChanges();

        return NoContent();
    }

    private bool StepExists(int id)
    {
        return _context.Step.Any(e => e.id == id);
    }
}
