using NUnit.Framework;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using recipes.Models;
using recipes.Providers;

namespace recipes.Recipes.Test;

[TestFixture]
public class RecipeProviderTest 
{
    [Test]
    public void testGetById()
    {
        var connection = new SqliteConnection("Data Source=/Users/mollywilson/molly.db");
        connection.Open();

    // should this be RecipeDBContext?
        var options = new DbContextOptionsBuilder<RecipeDBContext>().UseSqlite(connection).Options;

        using(var context = new RecipeDBContext(options))
        {
            context.Database.EnsureCreated();
        }

        using(var context = new RecipeDBContext(options))
        {
            context.Recipe.Add(new Recipe { 
                id = 20, 
                title = "Bowl of Cereal",
                description = "ertyuio",
                servingSize = 1,
                time = 1
            });
            context.SaveChanges();
        }

        using(var context = new RecipeDBContext(options))
        {
            var provider = new RecipeProvider(context);
            var recipe = provider.Get(20);

            Assert.AreEqual("Bowl of Cereal", recipe.title);
        }

    }
}