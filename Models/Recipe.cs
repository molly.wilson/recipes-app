using System.ComponentModel.DataAnnotations;

namespace recipes.Models;
public class Recipe
{
    [Key]
    public int id { get; set; }
    public string? title { get; set; }
    public string? description { get; set; }
    public int? servingSize { get; set; }
    public int? time { get; set; }
    public ICollection<Step>? method { get; set; } //list of the steps to take to make the recipe
    public ICollection<Ingredient>? ingredients { get; set; } //list of ingredients in the recipe
}