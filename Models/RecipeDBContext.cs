using Microsoft.EntityFrameworkCore;

namespace recipes.Models;

public class RecipeDBContext:DbContext
{
    public RecipeDBContext(DbContextOptions<RecipeDBContext> options):base(options)
    {
        
    }

    public DbSet<Recipe> Recipe { get; set; }

    public DbSet<Ingredient> Ingredient { get; set; }

    public DbSet<FoodItem> FoodItem { get; set; }

    public DbSet<Step> Step { get; set; }

}
