using System.ComponentModel.DataAnnotations;

namespace recipes.Models;
public class Step
{
    [Key]
    public int id { get; set; }
    public int? number { get; set; }
    public string? description { get; set; }
}