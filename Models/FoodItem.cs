using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace recipes.Models;
public class FoodItem
{
    [Key]
    public int id { get; set; }

    [Column(TypeName="nvarchar(100")]
    public string? name { get; set; }
}