using System.ComponentModel.DataAnnotations;

namespace recipes.Models;
public class Ingredient
{
    [Key]
    public int id { get; set; }
    public string? name { get; set; } //this is the name of a food item
    public string? amount { get; set; }
    public FoodItem foodItem { get; set; }
}