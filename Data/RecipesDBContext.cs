#nullable disable

using Microsoft.EntityFrameworkCore;

namespace recipes.Data;

    public class RecipesDBContext : DbContext
    {
        public RecipesDBContext (DbContextOptions<RecipesDBContext> options)
            : base(options)
        {
        }

        public DbSet<recipes.Models.Recipe> Recipe { get; set; }

        public DbSet<recipes.Models.Ingredient> Ingredient { get; set; }

        public DbSet<recipes.Models.FoodItem> FoodItem { get; set; }

        public DbSet<recipes.Models.Step> Step { get; set; }

    }
