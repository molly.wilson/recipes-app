using recipes.Models;

namespace recipes.Providers;

public class RecipeProvider
{
    private readonly RecipeDBContext recipeContext;

    public RecipeProvider(RecipeDBContext recipeContext)
    {
        this.recipeContext = recipeContext;
    }

    public Recipe Get(int id)
    {
        return recipeContext.Recipe.Where(e => e.id == id).FirstOrDefault();
    }
}
